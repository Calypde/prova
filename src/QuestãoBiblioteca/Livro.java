package QuestãoBiblioteca;

public class Livro extends Item {
	
	private String volume;
	private String autor;
	private String edicao;
	
	public Livro(String titulo, String editora, int anoPublicacao, String isbn, int valor, String volume, String autor, String edicao) {
		super(titulo, editora, anoPublicacao, isbn, valor);
		this.volume =  volume;
		this.autor = autor;
		this.edicao = edicao;
	}
	
	public void display() {
		System.out.println("Volume - " + this.volume);
		System.out.println("Autor - " + this.autor);
		System.out.println("Edição - " + this.edicao);
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEdicao() {
		return edicao;
	}

	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}

	
}
