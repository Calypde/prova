package Quest�oBiblioteca;

public class Biblioteca {

	public static void main(String[] args) {
		
		Livro book1 = new Livro("Iracema", "Typ. de Viana & Filhos", 1865, "9788516096939", 30, "�nico", "Jos� de Alencar", "1�");
		
				
		Arquivo estante = new Arquivo();
			
		estante.add(book1);
		
		//Display()
		for (Item b: estante) {
			System.out.println("-----Livro adicionando------");
			System.out.println("T�tulo - " + b.getTitulo());
			System.out.println("Editora - " + b.getEditora());
			System.out.println("Ano de Publica��o - " + b.getAnoPublicacao());
			System.out.println("ISBN - " + b.getIsbn());
			System.out.println("Pre�o - R$" + b.getValor());
			book1.display();
		}
		
	}


}
